const path= require('path')
const express= require('express')
const app= express();
const hbs=require('hbs');
const geoCode=require('./utils/geocde');
const forecast=require('./utils/forecast');

//Define path for express confif
const publicDirectorypath= path.join(__dirname,'../public');
const viewsPath= path.join(__dirname, '../templates/views')
const partialPath= path.join(__dirname,'../templates/partials')


//setup static directory to serve
app.use(express.static(publicDirectorypath));


//setup handlers engine and views location
app.set('views', viewsPath)
app.set('view engine', 'hbs');
hbs.registerPartials(partialPath);


app.get('',(req, res)=>{
    res.render('index',{
        title:"Weather",
        name:"Ravi Ranjan"
    });
})

app.get('/about',(req, res)=>{
    res.render('about',{
        title:"About Me",
        name:"Ravi Ranjan"
    });
})

app.get('/help',(req, res)=>{
    res.render('help',{
        title:"Help",
        name:"Ravi Ranjan"
    });
})



app.get('/weather',(req, res)=>{
    if(!req.query.address){
        return  res.send({
             error:"you must provide a address"
         })
     }else{
         geoCode(req.query.address,(error,result)=>{
             if(error){
                 return res.send({error});
             }
             forecast(result.latitude, result.longitude,(error, forecastResult)=>{
                 if(error){
                     return res.send({error});
                 }

                 res.send({
                     foreCast:forecastResult,
                     location:result.location,
                     address:req.query.address
                 })
             })
         })
     }
})

app.get('/product',(req,res)=>{
    console.log(req.query.search)
    if(!req.query.search){
       return  res.send({
            error:"you must provide a search term"
        })
    }
    res.send({
        products:[]
    })
})

app.get('/help/*', (req,res)=>{
    res.render('404page',{
        title:"404",
        name:"Ravi Ranjan",
        errorMessage:"Help article not found"
    })
})

app.get('*',(req, res)=>{
    res.render('404page',{
        title:"404",
        name:"Ravi Ranjan",
        errorMessage:"Page not found"
    })
})



app.listen(0807,()=>{
    console.log("server is up on port 0807!!")
})